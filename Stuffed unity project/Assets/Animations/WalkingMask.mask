%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: WalkingMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Base_CTRL
    m_Weight: 1
  - m_Path: Base_CTRL/L_Leg_CTRL
    m_Weight: 1
  - m_Path: Base_CTRL/R_Leg_CTRL
    m_Weight: 1
  - m_Path: Base_CTRL/Root_CTRL
    m_Weight: 0
  - m_Path: Base_CTRL/Root_CTRL/Head_CTRL
    m_Weight: 0
  - m_Path: Base_CTRL/Root_CTRL/Head_CTRL/L_Ear_CTRL
    m_Weight: 0
  - m_Path: Base_CTRL/Root_CTRL/Head_CTRL/R_Ear_CTRL
    m_Weight: 0
  - m_Path: Base_CTRL/Root_CTRL/L_Arm_CTRL
    m_Weight: 0
  - m_Path: Base_CTRL/Root_CTRL/R_Arm_CTRL
    m_Weight: 0
  - m_Path: Base_CTRL/Root_CTRL/R_Arm_CTRL/Weapon_CTRL
    m_Weight: 0
  - m_Path: Bear_Asset
    m_Weight: 1
  - m_Path: Root_JNT
    m_Weight: 1
  - m_Path: Root_JNT/Head_JNT
    m_Weight: 0
  - m_Path: Root_JNT/Head_JNT/L_Ear_JNT
    m_Weight: 0
  - m_Path: Root_JNT/Head_JNT/R_Ear_JNT
    m_Weight: 0
  - m_Path: Root_JNT/L_Arm_JNT
    m_Weight: 0
  - m_Path: Root_JNT/L_Arm_JNT/L_Hand_JNT
    m_Weight: 0
  - m_Path: Root_JNT/L_Leg_JNT
    m_Weight: 1
  - m_Path: Root_JNT/R_Arm_JNT
    m_Weight: 0
  - m_Path: Root_JNT/R_Arm_JNT/R_Hand_JNT
    m_Weight: 0
  - m_Path: Root_JNT/R_Leg_JNT
    m_Weight: 1
  - m_Path: Scissor_Asset
    m_Weight: 0
