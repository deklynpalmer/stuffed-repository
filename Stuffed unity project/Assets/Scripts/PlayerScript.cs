﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.SceneManagement;

public class PlayerScript : MonoBehaviour
{
    [Header("Camera:")]
    public Camera m_camera;
    public Transform m_movementTransform;

    [Tooltip("This is the layer in which the raycast will hit and only hit.")]
    public LayerMask m_rayLayer;

    [Header("Movement Variables:")]
    public float m_acceleration = 10.0f;
    public float m_moveSpeed = 50.0f;

    [Header("Rotation Variables:")]
    [Tooltip("The time in seconds it takes to rotate.")]
    public float m_rotationTime = 0.5f;

    [Header("For Play Testing!")]
    public int m_sceneID = 0;

    [Header("Blade Swing Variables:")]
    public float m_swingTime = 2.0f;
    private float m_swingTimeMax;

    [Header("Sounds:")]
    public AudioClip[] m_hitAudioClips;
    public AudioClip[] m_attackAudioClips;
    public AudioClip[] m_swingAudioClips;

    [Header("Power Ups:")]
    public LayerMask m_buttonLayerMask;
    public int m_scissorCount;
    public int m_scissorCountMax = 3;
    public int m_pillowCount;
    public int m_pillowCountMax = 3;

    public GameObject m_powerUpObject;
    public PowerUpProperties[] m_powerUpProperties;

    public GameObject m_pillow;
    public GameObject m_scissorEffect;
    private GameObject m_powerUpEntity = null;

    [Header("Invinciblity Timings:")]
    //public float m_invincibleTime = 1.0f;
    //private float m_invincibleTimeMax;
    //private bool isInvincible = false;

    [Header("Hit Particle System:")]
    public ParticleSystem m_particleSystem;

    public Material[] m_materials;

    [Header("Sound Object:")]
    public GameObject m_audioSources;

    [Header("Attack Timings:")]
    public float m_attackTime = 0.5f;
    private float m_attackTimeMax;

    private bool m_mousePressed = false;
    private bool m_hasAttacked = false;
    private bool m_dead = false;

    private bool m_key1Pressed = false;
    private bool m_key2Pressed = false;

    private Rigidbody m_rigidBody = null;
    private Health m_health = null;
    private AudioSource[] m_sources;

    private Animator m_animator = null;

    void Start()
    {
        GetComponents();
        ErrorCheck();

        m_sources = m_audioSources.GetComponents<AudioSource>();

        m_attackTimeMax = m_attackTime;
        //m_invincibleTimeMax = m_invincibleTime;
    }

    private void Update()
    {
        m_dead = m_health.IsEmpty();

        RotatePlayer();
        MovePlayer();
        Attack();

        if (m_dead)
            Die();

        m_key1Pressed = Input.GetKeyDown(KeyCode.Q);
        m_key2Pressed = Input.GetKeyDown(KeyCode.E);

        PowerUpAttack();

        if(m_mousePressed)
        {
            if(m_attackTime <= m_attackTimeMax / 2.0f && m_attackTime > (m_attackTimeMax * 0.46f))
                PlayAttackSound();

            m_attackTime -= Time.deltaTime;

            if(m_attackTime <= 0.0f)
            {
                m_attackTime = m_attackTimeMax;
                m_hasAttacked = false;
                m_mousePressed = false;
            }
        }

        //EasterEgg();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Finish")
        {
            Die();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Enemy")
        {
            if (other.GetComponent<EnemyScript>())
            {
                if(m_mousePressed && !m_hasAttacked)
                {
                    if (m_attackTime > 0.0f)
                    {
                        other.GetComponent<Health>().TakeDamage(1);
                        other.GetComponent<EnemyScript>().m_particles.Emit(5);
                        other.GetComponent<EnemyScript>().PlayHitSound();
                        m_hasAttacked = true;
                    }
                }
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision.collider.gameObject);
        if(collision.gameObject.tag == "Button")
        {
            PowerUpPickUp pickUp = collision.gameObject.GetComponent<PowerUpPickUp>();

            if(pickUp)
            {
                if (pickUp.m_type == PowerUpType.Scissor)
                    m_scissorCount++;

                else if (pickUp.m_type == PowerUpType.Pillow)
                    m_pillowCount++;

                Destroy(collision.gameObject);
            }
        }
    }

    private void PowerUpAttack()
    {
        // Consts
        if (m_pillowCount > m_pillowCountMax)
            m_pillowCount = m_pillowCountMax;

        if (m_scissorCount > m_scissorCountMax)
            m_scissorCount = m_scissorCountMax;

        if (!m_powerUpEntity)
        {
            if (m_key1Pressed)
            {
                if(m_scissorCount > 0)
                    ActivateScissorPowerUp();
            }

            else if (m_key2Pressed)
            {
                if (m_pillowCount > 0)
                    ActivatePillowPowerUp();
            }

            m_animator.SetBool("Scissor", false);
            m_animator.SetBool("Pillow", false);

            m_pillow.SetActive(false);
            m_scissorEffect.SetActive(false);
        }

        else
        {
            PowerUp powerUp = m_powerUpEntity.GetComponent<PowerUp>();

            m_animator.SetBool(powerUp.GetName(), true);

            if (powerUp.GetName() == "Scissor")
            {
                m_powerUpEntity.transform.position = transform.position;
                m_scissorEffect.SetActive(true);
            }

            else if (powerUp.GetName() == "Pillow")
            {
                m_powerUpEntity.transform.position = transform.position + transform.forward * m_powerUpProperties[1].m_range;
                m_pillow.SetActive(true);
            }
        }
    }

    void ActivatePillowPowerUp()
    {
        m_powerUpEntity = Instantiate(m_powerUpObject, transform.position, Quaternion.identity);
        m_powerUpEntity.GetComponent<PowerUp>().m_properties = m_powerUpProperties[1];
        m_pillowCount--;
    }

    void ActivateScissorPowerUp()
    {
        m_powerUpEntity = Instantiate(m_powerUpObject, transform.position, Quaternion.identity);
        m_powerUpEntity.GetComponent<PowerUp>().m_properties = m_powerUpProperties[0];
        m_scissorCount--;
    }

    private void RotatePlayer()
    {
        if (!m_powerUpEntity)
        {
            RaycastHit hit;
            Ray ray = m_camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, m_rayLayer))
            {
                Vector3 target = hit.point - transform.position;
                target.y = 0;

                Debug.DrawLine(ray.origin, hit.point, Color.blue);

                Quaternion rotation = Quaternion.LookRotation(target);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, m_rotationTime * Time.deltaTime);
            }
        }
    }

    private void MovePlayer()
    {
        if (!m_powerUpEntity || m_powerUpEntity.GetComponent<PowerUp>().m_properties.m_name == "Scissor")
        {
            transform.Translate(new Vector3(Input.GetAxis("Horizontal") * m_moveSpeed * Time.deltaTime, 0.0f, Input.GetAxis("Vertical") * m_moveSpeed * Time.deltaTime), m_movementTransform);

            m_animator.SetBool("isWalking", (Input.GetAxis("Horizontal") != 0.0f || Input.GetAxis("Vertical") != 0.0f)); // Move Transition

            if ((Input.GetAxis("Horizontal") != 0.0f || Input.GetAxis("Vertical") != 0.0f))
            {
                if (!m_sources[0].isPlaying)
                    m_sources[0].Play();
            }
        }

        //m_rigidBody.AddForce(new Vector3(Input.GetAxis("Horizontal") * m_moveSpeed * Time.deltaTime, 0.0f, Input.GetAxis("Vertical") * m_moveSpeed * Time.deltaTime), ForceMode.Acceleration);
    }

    private void ResetScene()
    {
        Destroy(gameObject);
        SceneManager.LoadScene(m_sceneID);
    }

    private void ErrorCheck()
    {
        if (!m_camera)
            Debug.LogError("Camera not Found!");

        if (!m_movementTransform)
            Debug.LogError("Transform for movement not Found!");

        if (!m_rigidBody)
            Debug.LogError("No RigidBody Found!");

        if (!m_health)
            Debug.LogError("Health not Found!");

        //if (!m_sources)
            //Debug.LogError("Audio Source not Found!");

        if (!m_animator)
            Debug.LogError("Animator not Found!");

        if (!m_particleSystem)
            Debug.LogError("Hit Particle System not Found!");
    }

    private void GetComponents()
    {
        m_rigidBody = GetComponent<Rigidbody>();
        m_health = GetComponent<Health>();

        // Reason for this is to get the animator thats in the rigged visual object
        m_animator = GetComponentInChildren<Animator>();
    }

    private void Attack()
    {
        // Needs Rework!
        if (!m_powerUpEntity)
            m_mousePressed = Input.GetMouseButton(0);

        else
            m_mousePressed = false;

        m_animator.SetBool("isAttacking", m_mousePressed);

        //if (m_mousePressed)
        //    PlaySound(m_swordSwing, m_swordSwingPitchMin, m_swordSwingPitchMax);
        //
        //m_bladeAnimator.SetBool("Attack", m_mousePressed);
    }

    //private void EasterEgg()
    //{
    //    if (!m_lampLight.enabled)
    //        m_moonLight.color = Color.Lerp(m_moonColour, Color.red, 100);
    //}

    public void PlaySound(AudioClip clip, float pitchMin, float pitchMax)
    {
        //m_source.clip = clip;
        //m_source.pitch = Random.Range(pitchMin, pitchMax);
        //m_source.Play();
    }

    public void PlayHitSound()
    {
        AudioClip hitSound = m_hitAudioClips[Random.Range(0, m_hitAudioClips.Length)];

        if(!m_sources[1].isPlaying)
        {
            if (m_sources[1].clip != hitSound)
                m_sources[1].clip = hitSound;

            m_sources[1].Play();
        }
    }

    public void PlayAttackSound()
    {
        AudioClip attackSound;

        if(m_hasAttacked)
            attackSound = m_attackAudioClips[Random.Range(0, m_attackAudioClips.Length)];
        else
            attackSound = m_swingAudioClips[Random.Range(0, m_swingAudioClips.Length)];

        if (!m_sources[2].isPlaying)
        {
            if (m_sources[2].clip != attackSound)
                m_sources[2].clip = attackSound;

            m_sources[2].Play();
        }
    }

    public void Die()
    {
        GameManager.UpdatePlayerStatistics();
        SaveSystem.SaveData();
        SaveSystem.LoadData();
        ResetScene();
    }
}
