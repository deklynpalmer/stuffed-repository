﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    public static void SaveData()
    {
        PlayerData data = new PlayerData();

        BinaryFormatter binFormatter = new BinaryFormatter();

        string path = Application.persistentDataPath + "/progress.sav";

        FileStream fs = new FileStream(path, FileMode.Create);

        binFormatter.Serialize(fs, data);
        fs.Close();
    }

    public static void LoadData()
    {
        string path = Application.persistentDataPath + "/progress.sav";

        if(File.Exists(path))
        {
            BinaryFormatter binFormatter = new BinaryFormatter();
            FileStream fs = new FileStream(path, FileMode.OpenOrCreate);

            PlayerData data = (PlayerData)binFormatter.Deserialize(fs);

            GameManager.m_personalbestKills = data.m_personalbestKills;
            GameManager.m_personalbestHighestScore = data.m_personalbestHighestScore;
            GameManager.m_personalbestMostFrankenstuffyKills = data.m_personalbestMostFrankenstuffyKills;
            GameManager.m_personalBestWaves = data.m_personalBestWaves;

            fs.Close();
        }

        else
        {
            Debug.LogError("No File found!");
            return;
        }
    }
}
