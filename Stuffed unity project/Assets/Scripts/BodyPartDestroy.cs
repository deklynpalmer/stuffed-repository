﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyPartDestroy : MonoBehaviour
{
    public float m_waitTime = 20.0f;
    public float m_scaleDownTime = 4.0f;
    public void Update()
    {
        if (m_waitTime > 0.0f)
            m_waitTime -= Time.deltaTime;

        else if(m_waitTime <= 0.0f)
        {
            float decrement = Time.deltaTime / m_scaleDownTime;
            if ((gameObject.transform.localScale.x - decrement) > 0.0f)
            {
                gameObject.transform.localScale -= new Vector3(decrement, decrement, decrement);
            }

            else
            {
                Destroy(gameObject);
            }
        }
    }
}
