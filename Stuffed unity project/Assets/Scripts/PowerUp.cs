﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Rendering;

public class PowerUp : MonoBehaviour
{
    [Header("Power Up:")]
    public PowerUpProperties m_properties;

    private float m_range;
    private float m_timer = 0.0f;
    private float m_duration;
    private int m_damage;
    private float m_force;
    private int m_killCount = 0;
    private int m_killCountMax;

    private string m_name;

    // Start is called before the first frame update
    void Start()
    {
        m_range = m_properties.m_range;
        m_duration = m_properties.m_duration;
        m_damage = m_properties.m_damage;
        m_force = m_properties.m_force;
        m_killCountMax = m_properties.m_killCount;

        m_name = m_properties.m_name;
    }

    // Update is called once per frame
    void Update()
    {
        m_timer += Time.deltaTime;

        AreaOfEffect();

        if (m_timer >= m_duration)
            Destroy(gameObject);
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, m_range);
    }

    void AreaOfEffect()
    {
        Collider[] m_hitColliders = Physics.OverlapSphere(transform.position, m_range);

        for(int i = 0; i < m_hitColliders.Length; ++i)
        {
            // Check if we have an enemy
            if(m_hitColliders[i].tag == "Enemy")
            {
                // If a kill count has been specified
                if (m_killCountMax > 0)
                {
                    // If the kill count is less than the max
                    if (m_killCount < m_killCountMax)
                    {
                        // Make the Enemy take damage
                        m_hitColliders[i].GetComponent<Health>().TakeDamage(m_damage);
                        m_hitColliders[i].GetComponent<EnemyScript>().PlayHitSound();
                        m_killCount++;
                    }
                }

                // If a kill count hasn't been specified
                else
                {
                    // Make the Enemy take damage
                    m_hitColliders[i].GetComponent<Health>().TakeDamage(m_damage);
                    
                }

                Vector3 opposite;
                opposite = (m_hitColliders[i].transform.position - transform.position).normalized;

                m_hitColliders[i].attachedRigidbody.AddForce(opposite * m_force);
                m_hitColliders[i].GetComponent<EnemyScript>().m_beenHit = true;
                m_hitColliders[i].GetComponent<EnemyScript>().PlayHitSound();
            }
        }
    }

    public string GetName()
    {
        return m_name;
    }
}
