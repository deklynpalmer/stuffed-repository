﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public int m_score;
    public int m_kills;
    public int m_wavesSurvived;
    public int m_buttonsCollected;
    public int m_frankenstuffyKills;

    public int m_personalbestKills;
    public int m_personalbestHighestScore;
    public int m_personalbestMostFrankenstuffyKills;
    public int m_personalBestWaves;

    public PlayerData()
    {
        m_score = GameManager.m_score;
        m_kills = GameManager.m_kills;
        m_wavesSurvived = GameManager.m_wavesSurvived;
        m_buttonsCollected = GameManager.m_buttonsCollected;
        m_frankenstuffyKills = GameManager.m_frankenstuffyKills;

        m_personalbestKills = GameManager.m_personalbestKills;
        m_personalbestHighestScore = GameManager.m_personalbestHighestScore;
        m_personalbestMostFrankenstuffyKills = GameManager.m_personalbestMostFrankenstuffyKills;
        m_personalBestWaves = GameManager.m_personalBestWaves;
    }
}
