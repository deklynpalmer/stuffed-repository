﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    // Recommit
    // For now we'll load the default scene ie Deklyn's scene (or whatever the best scene is)
    public int m_sceneID = 0;
    public GameObject m_statisticsScreen;

    private void Awake()
    {
     if(gameObject.activeSelf == false)
        {
            gameObject.SetActive(true);
        }
        Time.timeScale = 0;
    }

    // Start is called before the first frame update
    void Start()
    {
        GameManager.ResetStats();
        GameManager.LoadDataMenu();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void StartGame()
    {
        SceneManager.LoadScene(m_sceneID);
        Time.timeScale = 1;
    }

    public void ShowStatistics()
    {
        if (gameObject.activeSelf == true)
        {
            gameObject.SetActive(false);
            m_statisticsScreen.SetActive(true);
        }
    }


    public void QuitGame()
    {
        Application.Quit();
    }
}
