﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [Header("Spawners:")]
    public Spawner[] m_spawners;

    [Header("Enemies:")]
    public GameObject[] m_enemies;

    [Header("Player:")]
    public GameObject m_player;

    [Header("Bosses:")]
    public GameObject[] m_boss;

    [Header("Spawn Properties:")]
    public int m_wave = 0;
    public int m_spawnCountMax = 20;
    int m_spawnCountCurrent = 0;
    int m_spawnCount = 0;
    [Tooltip("This is the percentage of the max max number of enemies that will be ready for next wave.")]
    [Range(0.0f, 100.0f)]
    public float m_spawnCountIncrease = 15.0f;
    [Tooltip("In seconds of the wave spawn time.")]
    public float m_waveSpawnTimer = 30.0f;
    [Tooltip("In seconds of the wave cool down.")]
    public float m_waveCooldownTimer = 5.0f;
    [Tooltip("Percentage of the decrease in the spawn timer at the end of each wave.")]
    [Range(0.0f, 100.0f)]
    public float m_waveSpawnTimerDecrease = 1.5f;
    float m_waveCooldownTimerMax;
    float m_spawnTimerCurrent, m_spawnTimerMax;

    [Header("Easter Egg:")]
    public GameObject m_lamp;

    void Start()
    {
        m_waveCooldownTimerMax = m_waveCooldownTimer;
        m_spawnTimerCurrent =  m_waveSpawnTimer / (float)m_spawnCountMax;
        m_spawnTimerMax = m_spawnTimerCurrent;
    }

    void Update()
    {
        GameManager.m_wavesSurvived = m_wave;
        if(!EndOfWave())
        {
            if (m_spawnTimerCurrent > 0)
                m_spawnTimerCurrent -= Time.deltaTime;

            else
            {
                m_spawnTimerCurrent = m_spawnTimerMax;
                SpawnEnemies();
            }
        }
        else
        {
            if(EndOfCoolDown())
            {
                int tempSpawnCount = (int)Mathf.Floor((float)m_spawnCountMax * (1.0f + (m_spawnCountIncrease / 100.0f)));

                Debug.Log(tempSpawnCount);
                m_spawnCountMax = tempSpawnCount;

                m_spawnTimerCurrent = m_waveSpawnTimer / (float)m_spawnCountMax;
                m_spawnTimerMax = m_spawnTimerCurrent;
                m_spawnCount = 0;
                m_wave++;
            }
        }
    }

    public void DecreaseEnemyCount()
    {
        m_spawnCountCurrent--;
    }

    public void DecreaseBossCount()
    {
        m_spawnCountCurrent -= m_spawnCountMax / (m_wave / 5);
    }

    private bool EndOfWave()
    {
        return (m_spawnCountCurrent <= 0) && EndOfSpawn();
    }

    private bool EndOfCoolDown()
    {
        if(m_waveCooldownTimer > 0.0f)
        {
            m_waveCooldownTimer -= Time.deltaTime;
            return false;
        }

        else
        {
            m_waveCooldownTimer = m_waveCooldownTimerMax;
            return true;
        }
    }

    private void SpawnEnemies()
    {
        if (!EndOfSpawn())
        {
            if (m_wave % 5 != 0)
            {
                GameObject enemy = Instantiate(m_enemies[Random.Range(0, m_enemies.Length)], m_spawners[Random.Range(0, m_spawners.Length)].transform.position, Quaternion.identity);
                EnemyScript enemyScript = enemy.GetComponent<EnemyScript>();
                enemyScript.m_target = m_player.transform;
                enemyScript.m_spawner = this;

                m_spawnCount++;
                m_spawnCountCurrent++;
            }

            else
            {
                GameObject enemy = Instantiate(m_boss[Random.Range(0, m_boss.Length)], m_spawners[Random.Range(0, m_spawners.Length)].transform.position, Quaternion.identity);
                EnemyScript enemyScript = enemy.GetComponent<EnemyScript>();
                enemyScript.m_boss = true;
                enemyScript.m_target = m_player.transform;
                enemyScript.m_spawner = this;

                int spawnIncrease = (int)Mathf.Round(m_spawnCountMax * (5.0f / m_wave));
                Debug.Log(spawnIncrease);
                m_spawnCount += spawnIncrease;
                m_spawnCountCurrent += spawnIncrease;
            }
            
        }

        else
        {
            return;
        }
    }

    private bool EndOfSpawn()
    {
        return (m_spawnCount >= m_spawnCountMax);
    }
}