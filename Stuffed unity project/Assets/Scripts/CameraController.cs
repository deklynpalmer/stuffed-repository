﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [Header("Camera Position:")]
    public Transform m_cameraTransform;

    [Header("Camera Positioning:")]
    public Vector3 m_offset;
    [Tooltip("The time it takes in seconds to get from current camera position to player position.")]
    public float m_time;

    public void Update()
    {
        // All thats needed for moving the camera
        m_cameraTransform.position = Vector3.Lerp(m_cameraTransform.position, transform.position + m_offset, m_time * Time.deltaTime);
    }
}
