﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Animations;
using UnityEngine;

public class Collectable : MonoBehaviour
{
    [Header("Collectable Properties:")]
    public int m_scoreValue = 50;
    [Tooltip("Set this value to zero if collectable is for scroe only!")]
    public int m_healthValue = 0;

    [Header("Timings:")]
    [Tooltip("The time in seconds of how long it takes to get to the player!")]
    public float m_time = 1.0f;

    [Header("Visual Properties:")]
    public Mesh m_mesh;
    public RuntimeAnimatorController m_controller;
    public GameObject m_particleEffect;

    [Header("Sound:")]
    public AudioClip m_pickUpSound;
    public float m_pitchMin = 1.0f;
    public float m_pitchMax = 1.5f;

    void Start()
    {
        if (m_scoreValue <= 0)
            Debug.LogError($"Score is bellow or equal to zero! ({gameObject.name})");

        GetComponentInChildren<MeshFilter>().mesh = m_mesh;
        GetComponentInChildren<Animator>().runtimeAnimatorController = m_controller;
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            if(m_healthValue > 0)
            {
                Health m_health = collision.gameObject.GetComponent<Health>();

                if (m_health.GetHealth() < m_health.Max())
                    m_health.Heal(m_healthValue);

                else
                    GameManager.m_score += m_scoreValue;
            }

            else
            {
                GameManager.m_score += m_scoreValue;
                GameManager.m_buttonsCollected++;
            }

            GameObject particleEffect = Instantiate(m_particleEffect, transform.position, Quaternion.identity);
            AudioSource audioSource = particleEffect.GetComponent<AudioSource>();
            audioSource.clip = m_pickUpSound;
            audioSource.pitch = Random.Range(m_pitchMin, m_pitchMax);
            audioSource.Play();

            Destroy(gameObject);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Pick Up")
        {
            transform.position = Vector3.Lerp(transform.position, other.transform.position, Time.deltaTime / m_time);
        }
    }
}
