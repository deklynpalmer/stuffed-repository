﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CinematicEntry : MonoBehaviour
{

    [Header("Other Camera:")]
    public Transform m_mainCamera;
    public Transform m_canvas;
    public float m_timer = 0.1f;
    public float m_activationDist = 0.1f;

    [Header("In Game Refs:")]
    public PlayerScript m_player;
    public Transform m_spawners;

    private Vector3 m_velocity;

    private void Start()
    {
        m_player.enabled = false;
    }


    void Update()
    {
        if(Vector3.Magnitude(m_mainCamera.transform.position - transform.position) <= m_activationDist)
        {
            m_canvas.gameObject.SetActive(true);
            m_mainCamera.gameObject.SetActive(true);
        
            m_spawners.gameObject.SetActive(true);
            m_player.enabled = true;
            gameObject.SetActive(false);
        }

        transform.position = Vector3.SmoothDamp(transform.position, m_mainCamera.transform.position, ref m_velocity, m_timer);
        transform.rotation = Quaternion.Lerp(transform.rotation, m_mainCamera.rotation, 0.01f);
    }
}
