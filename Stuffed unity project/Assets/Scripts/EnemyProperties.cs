﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Enemy Properties", menuName = "Enemy Class Sets/Enemy Properties")]
public class EnemyProperties : ScriptableObject
{
    [Header("Enemy Properties:")]
    public float m_moveSpeed;
    [Range(1, 10)]
    public int m_damage;
    public int m_health;
    public int m_coinDropAmount;
}
