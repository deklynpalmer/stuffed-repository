﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

public class GameManager : MonoBehaviour
{
    // Recommit
    /// <summary>
    /// These are the things the game manager counts while the player is in a game.
    /// </summary>
    public static int m_score = 0;
    public static int m_kills = 0;
    public static int m_wavesSurvived = 0;
    public static int m_buttonsCollected = 0;
    public static int m_frankenstuffyKills = 0;

    public GameObject m_player;

    public GameObject m_summaryScreen;

    public GameObject m_enemy; // Temporary until the spawner is completed

    public Camera m_playerCamera;

    public int m_sceneIndex = 0;

    //private float m_testVal = 0;
    //private float m_testVal2 = 0;
    //private int m_testVal3 = 0;
    //private float m_testVal4 = 0;

    /// <summary>
    /// These are the overall stats of the player throughout their playtime
    /// </summary>
    public static int m_overallKills = 0;
    public static int m_overallButtonsCollected = 0;
    public static int m_overallFrankenstuffyKills = 0;
    public static int m_overallWavesSurvived = 0;

    /// <summary>
    /// These are the personal bests of the player
    /// </summary>
    public static int m_personalbestKills = 0;
    public static int m_personalbestHighestScore = 0;
    public static int m_personalbestMostFrankenstuffyKills = 0;
    public static int m_personalBestWaves = 0;


    private bool m_R_KeyPressed = false;
    //public GameObject m_button;
    //public GameObject m_player;
    // public DestroyButton dButton;
    // Start is called before the first frame update

    void Start()
    {
        
    }

    public static void LoadDataMenu()
    {
        SaveSystem.LoadData();
    }

    // Update is called once per frame
    void Update()
    {
        m_R_KeyPressed = Input.GetKeyDown(KeyCode.R);
        

        if (m_R_KeyPressed)
        {
            Instantiate(m_player, GetComponent<GameManager>().transform.position, Quaternion.identity); //Player respawn, doesn't 100% work yet. Not even sure it will be necessary as when the player dies they start all over again.
        }

        if (!m_player)
        {
            m_summaryScreen.SetActive(true);            
        }
    }

    public static void ResetStats()
    {
        m_score = 0;
        m_kills = 0;
        m_wavesSurvived = 0;
        m_buttonsCollected = 0;
        m_frankenstuffyKills = 0;
}
    
    public void RestartLevel()
    {
       // m_summaryScreen.SetActive(false); 
        SceneManager.LoadScene(m_sceneIndex);
    }

    public static void UpdatePlayerStatistics()
    {
        // Add the current session's things to the overall stats
        m_overallButtonsCollected = m_buttonsCollected;
        m_overallFrankenstuffyKills = m_frankenstuffyKills;
        m_overallKills = m_kills;
        m_overallWavesSurvived = m_wavesSurvived;

        //Check all the relevant stats and update personal bests
        if(m_kills > m_personalbestKills)
        {
            m_personalbestKills = m_kills;
        }
        if(m_frankenstuffyKills > m_personalbestMostFrankenstuffyKills)
        {
            m_personalbestMostFrankenstuffyKills = m_frankenstuffyKills;
        }
        if(m_score > m_personalbestHighestScore)
        {
            m_personalbestHighestScore = m_score;
        }

        if (m_wavesSurvived > m_personalBestWaves)
            m_personalBestWaves = m_wavesSurvived;
    }
    public void Quit()
    {
        Application.Quit();
    }
}
