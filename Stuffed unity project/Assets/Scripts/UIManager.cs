﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{

    [Header("Health Candies:")]
    public GameObject[] m_healthCandies;

    [Header("Player Health:")]
    public Health m_playerHealth;

    [Header("UI Numbers:")]
    public UICustomNumber m_score;
    public UICustomNumber m_waves;
    public UICustomNumber m_scissor;
    public UICustomNumber m_pillow;

    [Header("PowerUp Images:")]
    public Image m_pillowIcon;
    public Image m_scissorIcon;

    public Sprite m_pillowIconZero;
    public Sprite m_pillowIconDefault;

    public Sprite m_scissorIconZero;
    public Sprite m_scissorIconDefault;

    [Header("Pause Menu:")]
    public Transform m_pauseMenu;
    public Transform m_gameUI;
    public Volume m_profile;
    private DepthOfField m_field = null;

    public float m_defualtFocusDistance = 20.0f;

    private PlayerScript m_playerScript;
    public int m_mainMenuID = 1;


    void Start()
    {
        if (!m_playerHealth)
            Debug.LogError("No player health assigned!");

        if (m_healthCandies.Length == 0)
            Debug.LogError("No Health Candies objects assigned!");

        if (m_playerHealth.GetHealth() > m_healthCandies.Length)
            Debug.LogError($"Health specified is too high! Try Setting health to {m_healthCandies.Length}");

        m_playerScript = m_playerHealth.gameObject.GetComponent<PlayerScript>();

        if (m_profile.profile.TryGet(out m_field))
            Debug.Log("Depth found!");
        else
            Debug.LogError("Depth of Field not Found!");
    }


    void Update()
    {
        for(int i = 0; i < m_healthCandies.Length; ++i)
            m_healthCandies[i].SetActive(false);

        for(int i = 0; i < m_playerHealth.GetHealth(); ++i)
            m_healthCandies[i].SetActive(true);

        m_score.number = GameManager.m_score;
        m_waves.number = GameManager.m_wavesSurvived;

        m_pillow.number = m_playerScript.m_pillowCount;
        m_scissor.number = m_playerScript.m_scissorCount;

        if (m_pillow.number > 0)
        {
            m_pillowIcon.color = Color.white;
            m_pillowIcon.sprite = m_pillowIconDefault;
        }

        else
        {
            m_pillowIcon.color = Color.grey;
            m_pillowIcon.sprite = m_pillowIconZero;
        }

        if (m_scissor.number > 0)
        {
            m_scissorIcon.color = Color.white;
            m_scissorIcon.sprite = m_scissorIconDefault;
        }

        else
        {
            m_scissorIcon.color = Color.grey;
            m_scissorIcon.sprite = m_scissorIconZero;
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            m_gameUI.gameObject.SetActive(!m_gameUI.gameObject.activeSelf);
            m_pauseMenu.gameObject.SetActive(!m_pauseMenu.gameObject.activeSelf);
        }

        if(m_pauseMenu.gameObject.activeSelf)
        {
            Pause();
            m_field.focusDistance.value = 0.0f;
        }

        else
        {
            UnPause();
            m_field.focusDistance.value = m_defualtFocusDistance;
        }
    }

    public void Pause()
    {
        Time.timeScale = 0;
    }

    public void UnPause()
    {
        Time.timeScale = 1;
    }

    public void Continue()
    {
        m_gameUI.gameObject.SetActive(!m_gameUI.gameObject.activeSelf);
        m_pauseMenu.gameObject.SetActive(!m_pauseMenu.gameObject.activeSelf);
       
    }

    public void Exit()
    {
        SceneManager.LoadScene(m_mainMenuID);
        GameManager.ResetStats();
    }
}
