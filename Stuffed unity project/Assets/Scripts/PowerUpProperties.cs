﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Power Up Properties", menuName = "Power Ups/Power Up Properties")]
public class PowerUpProperties : ScriptableObject
{
    public float m_range;
    public float m_duration;
    public int m_damage;
    public float m_force;
    public int m_killCount;

    [Tooltip("This is the name/parameter inside of the animator to call to transission animations.")]
    public string m_name;
}
