﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverScript : MonoBehaviour
{
    [Header("Background Image:")]
    public Image m_foregroundImage;
    public float m_fadeTime = 2.0f;
    private float m_fadeTimeMax;
    public float m_showTime = 3.0f;
    private float m_showTimeMax;

    private bool m_fadeOut = false;

    [Header("Numbers:")]
    public UICustomNumber m_score;

    [Header("Scene ID:")]
    public int m_sceneID = 1;

    void Start()
    {
        m_score.number = GameManager.m_score;
        m_fadeTimeMax = m_fadeTime;
        m_showTimeMax = m_showTime;
    }

    void Update()
    {
        Color m_imageColour = m_foregroundImage.color;
        m_imageColour.a = m_fadeTime / m_fadeTimeMax;

        m_foregroundImage.color = m_imageColour;

        if (!m_fadeOut)
        {
            if (m_fadeTime > 0.0f)
                m_fadeTime -= Time.deltaTime;

            else
            {
                if (m_showTime > 0.0f)
                    m_showTime -= Time.deltaTime;

                else
                    m_fadeOut = true;
            }
        }

        else
        {
            if (m_fadeTime < m_fadeTimeMax)
                m_fadeTime += Time.deltaTime;

            else
            {
                SceneManager.LoadScene(m_sceneID);
            }
        }
    }
}
