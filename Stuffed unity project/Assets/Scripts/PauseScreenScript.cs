﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseScreenScript : MonoBehaviour
{
    // Recommit
    public int m_mainMenuSceneID;
    public GameObject m_gameManagerReference;

    private void Awake()
    {
        // This isn't 100% done, the time stop thing should be somewhere in update when the player presses a button or something
        //gameObject.SetActive(false);
        //Time.timeScale = 0;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Unpause()
    {
        gameObject.SetActive(false);
        Time.timeScale = 1;
    }

    public void BackButton()
    {
        GameManager.UpdatePlayerStatistics(); // This might not 100% work as the gamemanager gets deleted after the new scene is loaded. 
                                                                                     // So it might be necesarry for the values to be static or something so they carry over between scenes.
        SceneManager.LoadScene(m_mainMenuSceneID);
    }
}
