﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICustomNumber : MonoBehaviour
{
    [Header("Number Sprites:")]
    public Sprite[] m_numberSprites = new Sprite[10];

    [Header("Number Objects:")]
    public Image[] m_numbers;

    [Header("Number Offsets:")]
    public int m_numberWidth = 15;
    public float m_numberOffset = 1.5f;
    public Transform m_numbersTransform;

    [Header("Number Modular:")]
    public int m_modularValue = 10;

    [Header("Numeric Value:")]
    public int number = 0;

    void Update()
    {
        int objectsActive = 0;

        for (int i = 0; i < m_numbers.Length; ++i)
            if (m_numbers[i].gameObject.activeSelf)
                objectsActive++;

        int tempModularValue = m_modularValue;
        for (int i = m_numbers.Length - 1; i >= 0; --i)
        {
            float offset = m_numbersTransform.position.x + (float)m_numberWidth / 2.0f + (float)(m_numberWidth * (objectsActive - Mathf.Abs(i - m_numbers.Length))) + (float)(m_numberOffset * (objectsActive - Mathf.Abs(i - m_numbers.Length)));
            if (i == m_numbers.Length - 1)
            {
                int value = (number % tempModularValue);
                m_numbers[i].sprite = m_numberSprites[value];

                m_numbers[i].transform.position = new Vector3(offset, m_numbersTransform.position.y, m_numbersTransform.position.z);

                if (value == 0 && number < tempModularValue)
                    m_numbers[i].gameObject.SetActive(false);

                else
                    m_numbers[i].gameObject.SetActive(true);
            }

            else
            {
                int value = ((number % tempModularValue) - (number % 10)) / (tempModularValue / 10);
                m_numbers[i].sprite = m_numberSprites[value];

                m_numbers[i].transform.position = new Vector3(offset, m_numbersTransform.position.y, m_numbersTransform.position.z);

                if (value == 0 && number < tempModularValue)
                    m_numbers[i].gameObject.SetActive(false);

                else
                    m_numbers[i].gameObject.SetActive(true);
            }

            tempModularValue *= 10;
        }
    }
}
