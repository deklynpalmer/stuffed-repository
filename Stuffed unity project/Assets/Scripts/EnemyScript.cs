﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyScript : MonoBehaviour
{
    [Header("Enemy Property:")]
    public EnemyProperties m_properties;
    public Transform m_visualTransform;

    [Header("Enemy Variables:")]
    public Transform m_target;
    public float m_moveSpeed;
    public int m_damage;

    [Header("Buttons:")]
    public Transform m_buttonSpawn;
    public GameObject[] m_buttonTypes;
    public int m_buttonDropCount = 2;

    [Header("Death Variables:")]
    public Vector3 m_direction = Vector3.up;
    public float m_headPopForce = 5.0f;

    [Header("Body Part Spawn Points:")]
    public Transform m_headSpawnPoint;
    public Transform m_bodySpawnPoint;

    [Header("Body Parts:")]
    public GameObject m_head;
    public GameObject m_body;

    [Header("Particles:")]
    public ParticleSystem m_particles;

    [Header("Attack Timings:")]
    public float m_attackTime = 1.0f;
    private float m_attackTimeMax;
    public float m_getUpTime = 1.67f;
    private float m_getUpTimeMax;

    [Header("Spawner:")]
    [Tooltip("DO NOT EDIT THIS VALUE! This is so the actual spawn manager can give enemies a ref of itself to decrease the active spawn count.")]
    public SpawnManager m_spawner;

    [Header("Boss:")]
    public bool m_boss = false;

    private NavMeshAgent m_agent;
    private Animator m_animator;
    private Health m_health;
    private bool m_dead = false;
    private Rigidbody m_rb;
    public bool m_beenHit = false;

    private float m_activationTimer = 10.0f;
    private float m_activationTimerMax = 10.0f;

    void Start()
    {
        GetComponents();
        ErrorCheck();

        if (HasProperties())
            SetValuesFromProperties();

        m_attackTimeMax= m_attackTime;
        m_getUpTimeMax = m_getUpTime;

        m_rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        // POSSIBLE
        if(m_agent.enabled)
            m_agent.SetDestination(m_target.position);
        // Look Direction

        //Vector3 direction = ((new Vector3(m_target.position.x, 0.0f, m_target.position.z)) - new Vector3(transform.position.x, 0.0f, transform.position.z)).normalized;
        //m_rb.velocity += direction * m_moveSpeed * Time.deltaTime;
        //
        //transform.LookAt(m_target);
        //
        //Vector3.ClampMagnitude(m_rb.velocity, m_moveSpeed);

        if(m_beenHit)
        {
            m_agent.enabled = false;
            Physics.IgnoreCollision(gameObject.GetComponent<Collider>(), m_target.gameObject.GetComponent<Collider>());
        }

        else
        {
            m_agent.enabled = true;
        }

        if(!m_agent.enabled)
        {
            if (m_activationTimer > 0.0f)
                m_activationTimer -= Time.deltaTime;

            else
            {
                m_beenHit = false;
                m_activationTimer = m_activationTimerMax;
                
            }
        }

        else
        {
            if (m_activationTimer != m_activationTimerMax)
                m_activationTimer = m_activationTimerMax;
        }

        Attack();

        if (m_health.IsEmpty())
            m_dead = true;

        if (m_dead)
            Die();

        //Attack
    }

    // OUTDATED!
    //oid OnCollisionEnter(Collision collision)
    //
    //   if (collision.collider.tag == "Player")
    //   {
    //       //Destroy(collision.collider.gameObject);
    //       Vector3 opposite;
    //       opposite = (collision.contacts[0].point - transform.position).normalized;
    //
    //       PlayerScript player = collision.gameObject.GetComponent<PlayerScript>();
    //       collision.gameObject.GetComponent<Health>().TakeDamage(m_damage);
    //       
    //       player.m_particleSystem.GetComponent<ParticleSystemRenderer>().material = player.m_materials[Random.Range(0, player.m_materials.Length)];
    //       player.m_particleSystem.Play();
    //       player.PlaySound(player.m_hit, player.m_hitPitchMin, player.m_hitPitchMax);
    //
    //       if(collision.rigidbody.velocity == Vector3.zero)
    //           collision.rigidbody.AddForce(opposite * 15, ForceMode.Impulse);
    //   }
    //
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            m_animator.SetBool("attacking", true);
        }

        if (other.tag == "Finish")
            Die();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Vector3 opposite;
            opposite = (collision.transform.position - transform.position).normalized;
            opposite.y = 0.0f;
            
            PlayerScript player = collision.gameObject.GetComponent<PlayerScript>();
            collision.gameObject.GetComponent<Health>().TakeDamage(m_damage);
            
            player.m_particleSystem.GetComponent<ParticleSystemRenderer>().material = player.m_materials[Random.Range(0, player.m_materials.Length)];
            player.m_particleSystem.Play();
            player.PlayHitSound();
            
            collision.rigidbody.AddForce(opposite * 15, ForceMode.Impulse);
            collision.gameObject.GetComponent<Health>().TakeDamage(m_damage);
        }

        if(collision.gameObject.tag == "Env")
        {
            m_beenHit = false;
        }
    }

    private void GetComponents()
    {
        m_agent = GetComponent<NavMeshAgent>();
        m_health = GetComponent<Health>();
        m_animator = GetComponentInChildren<Animator>();
    }

    private void ErrorCheck()
    {
        if (!m_agent)
            Debug.LogError("No Agent Found!");

        if (!m_health)
            Debug.LogError("No Health Found!");

        if (!m_animator)
            Debug.LogError("No Animator Found");
    }

    private bool HasProperties()
    {
        return (m_properties);
    }

    private void SetValuesFromProperties()
    {
        m_moveSpeed = m_properties.m_moveSpeed;
        m_damage = m_properties.m_damage;
        m_buttonDropCount = m_properties.m_coinDropAmount;

        m_health.m_health = m_properties.m_health;

        m_agent.speed = m_moveSpeed;
    }

    private void Die()
    {
        SpawnBodyParts();
        SpawnButtons();

        if (!m_boss)
        {
            m_spawner.DecreaseEnemyCount();
            GameManager.m_score += 50;
            GameManager.m_kills++;
            
        }

        else
        {
            m_spawner.DecreaseBossCount();
            GameManager.m_score += 100;
            GameManager.m_frankenstuffyKills++;
            GameManager.m_kills++;
        }

        Destroy(gameObject);
    }

    private void SpawnButtons()
    {
        for (int i = 0; i < m_buttonDropCount; ++i)
        {
            GameObject button = Instantiate(m_buttonTypes[(Random.Range(0, 100) >= 85) ? Random.Range(0, m_buttonTypes.Length) : Random.Range(0, m_buttonTypes.Length - 2)], m_bodySpawnPoint.position, Quaternion.identity);
            button.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-4, 4), Random.Range(2, 5), Random.Range(-4, 4)), ForceMode.Impulse);
        }
    }

    private void SpawnBodyParts()
    {
        GameObject head = Instantiate(m_head, m_headSpawnPoint.position, Quaternion.identity);
        GameObject body = Instantiate(m_body, m_bodySpawnPoint.position, Quaternion.identity);

        head.transform.rotation = transform.rotation;
        body.transform.rotation = transform.rotation;

        head.GetComponent<Rigidbody>().AddForce(m_direction * m_headPopForce, ForceMode.Impulse);
    }

    private void Attack()
    {
        if (m_animator.GetBool("attacking"))
        {
            float m_time = Time.deltaTime;

            m_agent.speed = m_moveSpeed * 1.5f;

            if (m_attackTime > 0.0f)
                m_attackTime -= m_time;

            else if(m_attackTime <= 0.0f)
            {
                m_agent.speed = 0.5f;

                if (m_getUpTime > 0.0f)
                    m_getUpTime -= m_time;

                else if(m_getUpTime <= 0.0f)
                {
                    m_agent.speed = m_moveSpeed;
                    m_getUpTime = m_getUpTimeMax;
                    m_attackTime = m_attackTimeMax;

                    m_agent.speed = m_moveSpeed;
                    m_animator.SetBool("attacking", false);
                }
            }
        }
    }

    public void PlayHitSound()
    {
        AudioSource[] audioSources = GetComponents<AudioSource>();

        audioSources[1].Play();
    }
}
