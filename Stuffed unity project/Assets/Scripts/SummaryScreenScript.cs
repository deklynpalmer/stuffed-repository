﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class SummaryScreenScript : MonoBehaviour
{
    // Recommit
    public int m_mainMenuSceneID; // To load the main menu
    public int m_gameSceneID; // To load the game 

    public TextMeshProUGUI m_scoreMesh;
    public TextMeshProUGUI m_timeMesh;
    public TextMeshProUGUI m_killsMesh;
    //public TextMeshProGUI m_scoremesh;
    private void Awake()
    {
        //Don't show this at the start basically.
        gameObject.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        //We know what this is, which is basically change all the text in the TMPro
        m_scoreMesh.text = "Score: " + GameManager.m_score.ToString();
        m_killsMesh.text = "Kills: " + GameManager.m_kills.ToString();
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(m_gameSceneID);
    }

    public void BackButton()
    {
        SceneManager.LoadScene(m_mainMenuSceneID);
    }
}
