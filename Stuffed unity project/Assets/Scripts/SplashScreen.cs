﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour
{
    [Header("Colours:")]
    public Color m_logoColour;
    public Color m_textColour;

    [Header("Timings:")]
    public float m_fadeTime = 2.5f;
    private float m_fadeTimeMax;
    public float m_appearTime = 10.0f;

    [Header("Logo Stuff:")]
    public Image m_logo;
    public Text m_title;

    bool m_atEnd = false;

    void Start()
    {
        m_logoColour.a = 0.0f;
        m_logoColour.a = 0.0f;

        m_fadeTimeMax = m_fadeTime;
    }

    void Update()
    {
        float m_time = Time.deltaTime;

        m_title.color = m_textColour;
        m_logo.color = m_logoColour;

        if (!m_atEnd)
        {
            if(m_logoColour.a < 1.0f)
            {
                
                m_logoColour.a += m_time / m_fadeTimeMax;
                m_textColour.a += m_time / m_fadeTimeMax;
            }

            else
            {
                m_atEnd = true;
            }
        }

        else
        {
            if(m_atEnd)
            {
                if(m_appearTime > 0.0f)
                {
                    m_appearTime -= m_time;
                }

                else if(m_appearTime <= 0.0f)
                {
                    m_logoColour.a -= m_time / m_fadeTimeMax;
                    m_textColour.a -= m_time / m_fadeTimeMax;

                    if(m_logoColour.a <= 0.0f)
                    {
                        SceneManager.LoadScene("Main Menu");
                    }
                }
            }
        }
    }
}
