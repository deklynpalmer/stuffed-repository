﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningScript : MonoBehaviour
{
    public float m_strikeTime = 0.5f;

    public float m_nextStrikeTimeMax = 5.5f;
    public float m_nextStrikeTimeMin = 0.5f;

    public float m_lightNextStrikeTime = 1.3f;

    public float m_minIntensity = 0.0f;
    public float m_maxIntensity = 2.0f;

    private Light m_light;
    private AudioSource m_source;
    // Start is called before the first frame update
    void Start()
    {
        m_light = GetComponent<Light>();
        m_source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(m_light)
        {
            if (m_lightNextStrikeTime > 0.0f)
            {
                if(m_light.intensity > 0.0f)
                    m_light.intensity -= 0.125f;
                else
                    m_light.intensity = 0.0f;

                m_lightNextStrikeTime -= Time.deltaTime;
            }

            else if(m_lightNextStrikeTime <= 0.0f)
            {
                if (m_strikeTime > 0.0f)
                {
                    m_light.intensity = Random.Range(m_minIntensity, m_maxIntensity);
                    m_strikeTime -= Time.deltaTime;
                    
                    // m_source.volume = m_strikeTime;
                }

                else if (m_strikeTime <= 0.0f && m_lightNextStrikeTime <= 0.0f) 
                {
                    // m_source.Stop();
                    //m_source.volume = 1.0f;
                    m_source.pitch = Random.Range(0.5f, 1.3f);
                    m_strikeTime = Random.Range(0.05f, 1.0f);

                    m_lightNextStrikeTime = Random.Range(m_nextStrikeTimeMin, m_nextStrikeTimeMax);

                    m_source.Play();

                }
            }
        } 
    }
}
