﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StatisticsScreenScript : MonoBehaviour
{
    // Recommit
    public GameObject m_mainMenu;
    public UICustomNumber m_totalKills;
    public UICustomNumber m_buttonsCollected;
    public UICustomNumber m_frankenstuffyKills;
    public UICustomNumber m_totalWaves;

    public UICustomNumber m_highestScore;
    public UICustomNumber m_highestKills;
    public UICustomNumber m_highestFrankentstuffyKills;
    public UICustomNumber m_mostWaves;

    private void Awake()
    {
        gameObject.SetActive(false);

        
        //All this could be moved to the update function or make a new update function that updates all the values in the statistics screen 

        //Grab all the overall stats from the gamemanager and put them into the text boxes
        m_totalKills.number = GameManager.m_overallKills;
        m_buttonsCollected.number = GameManager.m_overallButtonsCollected;
        m_frankenstuffyKills.number = GameManager.m_overallFrankenstuffyKills;
        m_totalWaves.number = GameManager.m_overallWavesSurvived;

        SaveSystem.LoadData();
        //Grab all the personal bests from the gamemanger and put them into the text boxes
        m_highestScore.number = GameManager.m_personalbestHighestScore;
        m_highestKills.number = GameManager.m_personalbestKills;
        m_highestFrankentstuffyKills.number = GameManager.m_personalbestMostFrankenstuffyKills;
        m_mostWaves.number = GameManager.m_personalBestWaves;

    }

    public void BackToMenu()
    {
        gameObject.SetActive(false);
        m_mainMenu.SetActive(true);   
    }
}
