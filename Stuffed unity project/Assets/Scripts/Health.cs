﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class Health : MonoBehaviour
{
    [Header("Health Properties:")]
    public int m_health = 100;
    int m_maxHealth;

    void Start()
    {
        // If health hasn't been set, Report that the object contains zero health!
        if (m_health <= 0)
        {
            // Early out
            Debug.LogError($"Health has not been set in {gameObject.name}!");
            return;
        }

        // Other than that set the max health to the current set health.
        m_maxHealth = m_health;
    }

    public void TakeDamage(int damage)
    {
        if (m_health - damage <= 0)
            m_health = 0;

        else
            m_health -= damage;
    }

    /// <summary>
    /// Checks if the object is out of health.
    /// </summary>
    /// <returns>Returns weather or not the health is empty.</returns>
    public bool IsEmpty()
    {
        return (m_health <= 0);
    }

    /// <summary>
    /// Adds health. (Handles overflowing)
    /// </summary>
    /// <param name="healValue">Amount of health to add.</param>
    public void Heal(int healValue)
    {
        if ((m_health + healValue) >= m_maxHealth)
            m_health = m_maxHealth;

        else
            m_health += healValue;
    }

    public int GetHealth()
    {
        return m_health;
    }

    public int Max()
    {
        return m_maxHealth;
    }
}
