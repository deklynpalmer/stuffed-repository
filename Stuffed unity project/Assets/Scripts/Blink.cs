﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blink : MonoBehaviour
{
    [Header("Colours:")]
    public Color m_primaryColour;
    public Color m_secondaryColour;

    [Header("Timings:")]
    public float m_time = 0.3f;

    private Light m_light;

    void Start()
    {
        m_light = GetComponent<Light>();

        if (!m_light)
            Debug.Log("No Light was Found!");

        m_light.color = m_primaryColour;
    }

    void Update()
    {
        m_light.color = Color.Lerp(m_primaryColour, m_secondaryColour, Mathf.PingPong(Time.time, m_time));
    }
}
