﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PowerUpType
{
    Scissor,
    Pillow
}

public class PowerUpPickUp : MonoBehaviour
{
    public PowerUpType m_type;
    public AudioClip m_sound;
    public GameObject m_particlePickup;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            PlayerScript playerScript = collision.gameObject.GetComponent<PlayerScript>();
            GameObject collect = Instantiate(m_particlePickup, transform.position, Quaternion.identity);
            collect.GetComponent<AudioSource>().clip = m_sound;
            collect.GetComponent<AudioSource>().Play();
        }
    }
}
